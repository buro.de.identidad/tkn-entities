package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieDireNest;
import com.teknei.bid.persistence.entities.BidClieDireNestPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidDireNestRepository extends JpaRepository<BidClieDireNest, BidClieDireNestPK> {
}
