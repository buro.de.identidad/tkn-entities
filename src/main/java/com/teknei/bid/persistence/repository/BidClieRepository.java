package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Amaro on 28/07/2017.
 */
public interface BidClieRepository extends JpaRepository<BidClie, Long> {

    @Modifying
    @Query("UPDATE BidClie b SET b.idTipo = :idTipo WHERE b.idClie = :idClie")
    void updateIdTipo(@Param("idClie") Long idClie, @Param("idTipo") Integer idTipo);
    
    @Modifying
    @Query("UPDATE BidClie b SET b.idclierel = :idclierel WHERE b.idClie = :idClie")
    void updateIdclierel(@Param("idClie") Long idClie, @Param("idclierel") Long idclierel);

    @Transactional
    @Procedure(name = "bid.bid_verify_user")
    String bid_verify_user(@Param("p_user") String p_user, @Param("p_pass") String p_pass);
    
    @Query(value = "select nextval('bid.bid_usuarios_seq')", nativeQuery = true)
    Long getUsuarioId();

}
