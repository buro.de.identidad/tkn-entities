package com.teknei.bid.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.BidClieReasonFinger;

public interface BidClieReasonFingerRepository extends JpaRepository<BidClieReasonFinger, Long> {

    List<BidClieReasonFinger> findAllByIdClie(Long idClie);


}