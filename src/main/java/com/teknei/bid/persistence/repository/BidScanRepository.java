package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidScan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidScanRepository extends JpaRepository<BidScan, String> {

    BidScan findByIdRegi(Long id);

    BidScan findByScanId(String scanId);

}