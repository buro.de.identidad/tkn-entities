package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieRegProcStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BidRegProcStatusRepository extends JpaRepository<BidClieRegProcStatus, Long> {

    List<BidClieRegProcStatus> findAllByIdClie(Long idClie);

    @Modifying
    @Query("UPDATE BidClieRegProcStatus b set b.curp = :curp WHERE b.idClie = :idClie")
    void updateCurp(@Param("curp") String curp, @Param("idClie") Long idClie);

}