package com.teknei.bid.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.BidClieRegConsultIne;

public interface BidClieRegConsultIneRepository extends JpaRepository<BidClieRegConsultIne, Long> {

    List<BidClieRegConsultIne> findByidClie(Long idOperation);

}