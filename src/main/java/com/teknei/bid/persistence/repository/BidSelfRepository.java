package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidSelf;
import com.teknei.bid.persistence.entities.BidSelfPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidSelfRepository extends JpaRepository<BidSelf, BidSelfPK> {
}
