package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieFirm;
import com.teknei.bid.persistence.entities.BidClieFirmPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieFirmRepository extends JpaRepository<BidClieFirm, BidClieFirmPK> {

    List<BidClieFirm> findAllByIdClieAndIdEsta(Long idClie, Integer idEsta);

    BidClieFirm findByIdClieAndIdTipoFirm(Long idClie, Long idTipoFirm);

}