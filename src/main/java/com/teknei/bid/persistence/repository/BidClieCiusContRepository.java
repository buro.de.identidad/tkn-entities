package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieCiusCont;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieCiusContRepository extends JpaRepository<BidClieCiusCont, Long> {

    List<BidClieCiusCont> findAllByIdClie(Long idClie);

}