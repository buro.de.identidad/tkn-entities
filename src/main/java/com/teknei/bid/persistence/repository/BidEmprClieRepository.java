package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidEmprClie;
import com.teknei.bid.persistence.entities.BidEmprCliePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidEmprClieRepository extends JpaRepository<BidEmprClie, BidEmprCliePK> {
}
