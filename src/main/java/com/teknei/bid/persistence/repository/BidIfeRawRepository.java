package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieIfeIneBita;
import com.teknei.bid.persistence.entities.BidClieIfeIneBitaPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidIfeRawRepository extends JpaRepository<BidClieIfeIneBita, BidClieIfeIneBitaPK> {
}
