package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_muni", schema = "bid", catalog = "bid")
@IdClass(BidMuniPK.class)
public class BidMuni {
    private String idPais;
    private String idEstd;
    private String idMuni;
    private String nomMuni;

    @Id
    @Column(name = "id_pais")
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Id
    @Column(name = "id_estd")
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Id
    @Column(name = "id_muni")
    public String getIdMuni() {
        return idMuni;
    }

    public void setIdMuni(String idMuni) {
        this.idMuni = idMuni;
    }

    @Basic
    @Column(name = "nom_muni")
    public String getNomMuni() {
        return nomMuni;
    }

    public void setNomMuni(String nomMuni) {
        this.nomMuni = nomMuni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidMuni bidMuni = (BidMuni) o;
        return Objects.equals(idPais, bidMuni.idPais) &&
                Objects.equals(idEstd, bidMuni.idEstd) &&
                Objects.equals(idMuni, bidMuni.idMuni) &&
                Objects.equals(nomMuni, bidMuni.nomMuni);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd, idMuni, nomMuni);
    }
}
