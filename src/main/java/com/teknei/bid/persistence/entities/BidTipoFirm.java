package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_tipo_firm", schema = "bid", catalog = "bid")
public class BidTipoFirm {
    private Long idTipoFirm;
    private String codTipoFirm;
    private String descTipoFirm;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_tipo_firm")
    public Long getIdTipoFirm() {
        return idTipoFirm;
    }

    public void setIdTipoFirm(Long idTipoFirm) {
        this.idTipoFirm = idTipoFirm;
    }

    @Basic
    @Column(name = "cod_tipo_firm")
    public String getCodTipoFirm() {
        return codTipoFirm;
    }

    public void setCodTipoFirm(String codTipoFirm) {
        this.codTipoFirm = codTipoFirm;
    }

    @Basic
    @Column(name = "desc_tipo_firm")
    public String getDescTipoFirm() {
        return descTipoFirm;
    }

    public void setDescTipoFirm(String descTipoFirm) {
        this.descTipoFirm = descTipoFirm;
    }

    @Basic
    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidTipoFirm that = (BidTipoFirm) o;
        return Objects.equals(idTipoFirm, that.idTipoFirm) &&
                Objects.equals(codTipoFirm, that.codTipoFirm) &&
                Objects.equals(descTipoFirm, that.descTipoFirm) &&
                Objects.equals(idEsta, that.idEsta) &&
                Objects.equals(idTipo, that.idTipo) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTipoFirm, codTipoFirm, descTipoFirm, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
