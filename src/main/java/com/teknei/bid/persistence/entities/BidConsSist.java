package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_cons_sist", schema = "bid", catalog = "bid")
public class BidConsSist {
    private Long idConsSist;
    private String codConsSist;
    private String valConsSist;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_cons_sist")
    public Long getIdConsSist() {
        return idConsSist;
    }

    public void setIdConsSist(Long idConsSist) {
        this.idConsSist = idConsSist;
    }

    @Basic
    @Column(name = "cod_cons_sist")
    public String getCodConsSist() {
        return codConsSist;
    }

    public void setCodConsSist(String codConsSist) {
        this.codConsSist = codConsSist;
    }

    @Basic
    @Column(name = "val_cons_sist")
    public String getValConsSist() {
        return valConsSist;
    }

    public void setValConsSist(String valConsSist) {
        this.valConsSist = valConsSist;
    }

    @Basic
    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidConsSist that = (BidConsSist) o;
        return Objects.equals(idConsSist, that.idConsSist) &&
                Objects.equals(codConsSist, that.codConsSist) &&
                Objects.equals(valConsSist, that.valConsSist) &&
                Objects.equals(idEsta, that.idEsta) &&
                Objects.equals(idTipo, that.idTipo) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idConsSist, codConsSist, valConsSist, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
