package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_dire", schema = "bid", catalog = "bid")
@IdClass(BidClieDirePK.class)
public class BidClieDire {
    private long idClie;
    private long idDire;
    private String idPais;
    private String idEstd;
    private String idMuni;
    private String idLoca;
    private String cp;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String call;
    private String noExt;
    private String noInt;
    private String col;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_dire")
    public long getIdDire() {
        return idDire;
    }

    public void setIdDire(long idDire) {
        this.idDire = idDire;
    }

    @Basic
    @Column(name = "id_pais")
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Basic
    @Column(name = "id_estd")
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Basic
    @Column(name = "id_muni")
    public String getIdMuni() {
        return idMuni;
    }

    public void setIdMuni(String idMuni) {
        this.idMuni = idMuni;
    }

    @Basic
    @Column(name = "id_loca")
    public String getIdLoca() {
        return idLoca;
    }

    public void setIdLoca(String idLoca) {
        this.idLoca = idLoca;
    }

    @Basic
    @Column(name = "cp")
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "call")
    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    @Basic
    @Column(name = "no_ext")
    public String getNoExt() {
        return noExt;
    }

    public void setNoExt(String noExt) {
        this.noExt = noExt;
    }

    @Basic
    @Column(name = "no_int")
    public String getNoInt() {
        return noInt;
    }

    public void setNoInt(String noInt) {
        this.noInt = noInt;
    }

    @Basic
    @Column(name = "col")
    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieDire that = (BidClieDire) o;
        return idClie == that.idClie &&
                idDire == that.idDire &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(idPais, that.idPais) &&
                Objects.equals(idEstd, that.idEstd) &&
                Objects.equals(idMuni, that.idMuni) &&
                Objects.equals(idLoca, that.idLoca) &&
                Objects.equals(cp, that.cp) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(call, that.call) &&
                Objects.equals(noExt, that.noExt) &&
                Objects.equals(noInt, that.noInt) &&
                Objects.equals(col, that.col) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idDire, idPais, idEstd, idMuni, idLoca, cp, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, call, noExt, noInt, col, usrOpeCrea, usrOpeModi);
    }
}
