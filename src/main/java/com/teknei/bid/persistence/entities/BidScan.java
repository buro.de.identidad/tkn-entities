package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_scan", schema = "bid", catalog = "bid")
public class BidScan {
    private String scanId;
    private String data;
    private long idRegi;
    private Timestamp fchCrea;
    private String usrCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "scan_id")
    public String getScanId() {
        return scanId;
    }

    public void setScanId(String scanId) {
        this.scanId = scanId;
    }

    @Basic
    @Column(name = "data")
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Basic
    @Column(name = "id_regi")
    public long getIdRegi() {
        return idRegi;
    }

    public void setIdRegi(long idRegi) {
        this.idRegi = idRegi;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidScan scan = (BidScan) o;
        return idRegi == scan.idRegi &&
                Objects.equals(scanId, scan.scanId) &&
                Objects.equals(data, scan.data) &&
                Objects.equals(fchCrea, scan.fchCrea) &&
                Objects.equals(usrCrea, scan.usrCrea) &&
                Objects.equals(usrModi, scan.usrModi) &&
                Objects.equals(fchModi, scan.fchModi) &&
                Objects.equals(usrOpeCrea, scan.usrOpeCrea) &&
                Objects.equals(usrOpeModi, scan.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(scanId, data, idRegi, fchCrea, usrCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
