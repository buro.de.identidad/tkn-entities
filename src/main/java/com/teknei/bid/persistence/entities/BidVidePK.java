package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidVidePK implements Serializable {
    private long idClie;
    private long idTasVide;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_tas_vide")
    @Id
    public long getIdTasVide() {
        return idTasVide;
    }

    public void setIdTasVide(long idTasVide) {
        this.idTasVide = idTasVide;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidVidePK bidVidePK = (BidVidePK) o;
        return idClie == bidVidePK.idClie &&
                idTasVide == bidVidePK.idTasVide;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idTasVide);
    }
}
