package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidTelPK implements Serializable {
    private long idClie;
    private String tele;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "tele")
    @Id
    public String getTele() {
        return tele;
    }

    public void setTele(String tele) {
        this.tele = tele;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidTelPK bidTelPK = (BidTelPK) o;
        return idClie == bidTelPK.idClie &&
                Objects.equals(tele, bidTelPK.tele);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, tele);
    }
}
