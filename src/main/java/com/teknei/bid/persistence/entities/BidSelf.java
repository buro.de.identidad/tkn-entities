package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_self", schema = "bid", catalog = "bid")
@IdClass(BidSelfPK.class)
public class BidSelf {
    private long idClie;
    private long idSelf;
    private Short porcComp;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_self")
    public long getIdSelf() {
        return idSelf;
    }

    public void setIdSelf(long idSelf) {
        this.idSelf = idSelf;
    }

    @Basic
    @Column(name = "porc_comp")
    public Short getPorcComp() {
        return porcComp;
    }

    public void setPorcComp(Short porcComp) {
        this.porcComp = porcComp;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidSelf bidSelf = (BidSelf) o;
        return idClie == bidSelf.idClie &&
                idSelf == bidSelf.idSelf &&
                idEsta == bidSelf.idEsta &&
                idTipo == bidSelf.idTipo &&
                Objects.equals(porcComp, bidSelf.porcComp) &&
                Objects.equals(usrCrea, bidSelf.usrCrea) &&
                Objects.equals(fchCrea, bidSelf.fchCrea) &&
                Objects.equals(usrModi, bidSelf.usrModi) &&
                Objects.equals(fchModi, bidSelf.fchModi) &&
                Objects.equals(usrOpeCrea, bidSelf.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidSelf.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idSelf, porcComp, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
