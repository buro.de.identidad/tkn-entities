package com.teknei.bid.persistence.entities;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @author AJGD
 * Entidad relacionada a la tabla encargada de validar la vigencia y bloquedo de accesos
 *
 */
@Entity
@Table(name = "bid_vali_usua", schema = "bid", catalog = "bid")
public class BidValUsr {
    
    private long user;
    private long activo;
    private Timestamp f_vigencia;
    private int intentos;
    private Timestamp f_bloqueo;
    private String anteriores;
    
    @Id 
    @Column(name = "id_clie",unique = true, nullable = false)
	public long getUser() {
		return user;
	}

	public void setUser(long user) {
		this.user = user;
	}
	
    @Basic
    @Column(name = "activo", nullable = false, columnDefinition = "1") 
	public long getActivo() {
		return activo;
	}

	public void setActivo(long activo) {
		this.activo = activo;
	}

	@Basic
    @Column(name = "f_expiracion", nullable = false, columnDefinition = "(now() + '3 mons'::interval)") 
	public Timestamp getFvigencia() {
		return f_vigencia;
	}

	public void setFvigencia(Timestamp f_vigencia) {
		this.f_vigencia = f_vigencia;
	}

	@Basic
    @Column(name = "no_intentos", nullable = false, columnDefinition = "0")
	public int getIntentos() {
		return intentos;
	}

	public void setIntentos(int intentos) {
		this.intentos = intentos;
	}

	@Basic
    @Column(name = "f_bloqueo")
	public Timestamp getFbloqueo() {
		return f_bloqueo;
	}

	public void setFbloqueo(Timestamp f_bloqueo) {
		this.f_bloqueo = f_bloqueo;
	}
	

	@Basic
    @Column(name = "anteriores")
	public String getAnteriores() {
		return anteriores;
	}

	public void setAnteriores(String f_bloqueo) {
		this.anteriores = f_bloqueo;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidValUsr bidClie = (BidValUsr) o;
        return this.user == bidClie.user &&
        	   this.activo == bidClie.activo &&
               this.f_vigencia == bidClie.f_vigencia &&
               this.intentos == bidClie.intentos &&
               this.f_bloqueo == bidClie.f_bloqueo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user,activo,f_vigencia,intentos, f_bloqueo);
    }
    
}
