package com.teknei.bid.persistence.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "bid_service_configuration", schema = "bid", catalog = "bid")
public class BidServiceConfig 
{
	private long   operationid;
	private int    settingsversion;  //SETTINGS_VERSION
	private String urlidscan;        // URL_ID_SCAN
	private String licenseidscan;    // LICENSE_ID_SCAN
	private String urlteknei;        // URL_TEKNEI
	private String urlmobbsign;      // URL_MOBBSIGN
	private String licenseobbsign;  // MOBBSIGN_LICENSE
	private String urlauthaccess;    // URL_AUTHACCESS
	private String fingerprintreader;// FINGERPRINT_READER
	private String idcompany;        // ID_ENTERPRICE
	private String timeoutservices;  // TIMEOUT_SERVICES
	private String timelatency;      // TIME_WAIT_LATENCY
	private String urlhostbid;       //URL_HOST_BID
	private String urlportbid;       //URL_PORT_BID
	private String status;       //URL_PORT_BID
	private String aviPriv;  //Aviso de privacidad
	private String nfiq;  //Sensibilidad de lector biometrico
	
 
	
	@Id
    @Column(name = "operationid")
	public long getOperationid() {
		return operationid;
	}
    @Basic
    @Column(name = "settingsversion")
	public int getSettingsversion() {
		return settingsversion;
	}
    @Basic
    @Column(name = "urlidscan")
	public String getUrlidscan() {
		return urlidscan;
	}
    @Basic
    @Column(name = "licenseidscan")
	public String getLicenseidscan() {
		return licenseidscan;
	}
    @Basic
    @Column(name = "urlteknei")
	public String getUrlteknei() {
		return urlteknei;
	}
    @Basic
    @Column(name = "urlmobbsign")
	public String getUrlmobbsign() {
		return urlmobbsign;
	}
    @Basic
    @Column(name = "licenseobbsign")
	public String getLicenseobbsign() {
		return licenseobbsign;
	}
    @Basic
    @Column(name = "urlauthaccess")
	public String getUrlauthaccess() {
		return urlauthaccess;
	}
    @Basic
    @Column(name = "fingerprintreader")
	public String getFingerprintreader() {
		return fingerprintreader;
	}
    @Basic
    @Column(name = "idcompany")
	public String getIdcompany() {
		return idcompany;
	}
    @Basic
    @Column(name = "timeoutservices")
	public String getTimeoutservices() {
		return timeoutservices;
	}
    @Basic
    @Column(name = "timelatency")
	public String getTimelatency() {
		return timelatency;
	}
    @Basic
    @Column(name = "urlhostbid")
	public String getUrlhostbid() {
		return urlhostbid;
	}
    @Basic
    @Column(name = "urlportbid")
	public String getUrlportbid() {
		return urlportbid;
	}
    @Basic
    @Column(name = "status")
	public String getStatus() {
		return status;
	}
    @Basic
    @Column(name = "avi_priv")
    public String getAviPriv() {
 		return aviPriv;
 	} 
    
    @Basic
    @Column(name = "nfiq")
    public String getNfiq() {
 		return nfiq;
 	}    
    
	public void setOperationid(long operationid) {
		this.operationid = operationid;
	}
	public void setSettingsversion(int settingsversion) {
		this.settingsversion = settingsversion;
	}
	public void setUrlidscan(String urlidscan) {
		this.urlidscan = urlidscan;
	}
	public void setLicenseidscan(String licenseidscan) {
		this.licenseidscan = licenseidscan;
	}
	public void setUrlteknei(String urlteknei) {
		this.urlteknei = urlteknei;
	}
	public void setUrlmobbsign(String urlmobbsign) {
		this.urlmobbsign = urlmobbsign;
	}
	public void setLicenseobbsign(String licenseobbsign) {
		this.licenseobbsign = licenseobbsign;
	}
	public void setUrlauthaccess(String urlauthaccess) {
		this.urlauthaccess = urlauthaccess;
	}
	public void setFingerprintreader(String fingerprintreader) {
		this.fingerprintreader = fingerprintreader;
	}
	public void setIdcompany(String idcompany) {
		this.idcompany = idcompany;
	}
	public void setTimeoutservices(String timeoutservices) {
		this.timeoutservices = timeoutservices;
	}
	public void setTimelatency(String timelatency) {
		this.timelatency = timelatency;
	}
	public void setUrlhostbid(String urlhostbid) {
		this.urlhostbid = urlhostbid;
	}
	public void setUrlportbid(String urlportbid) {
		this.urlportbid = urlportbid;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setAviPriv(String aviPriv) {
		this.aviPriv =  aviPriv;
	}
	public void setNfiq(String nfiq) {
		this.nfiq =  nfiq;
	}
}
