package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_aud_tab", schema = "bid", catalog = "bid")
public class BidAudTab {
    private long idAudTab;
    private String esqAud;
    private String tabAud;
    private String tipoOpe;
    private Timestamp fchOpe;
    private String valAnt;
    private String valPost;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_aud_tab")
    public long getIdAudTab() {
        return idAudTab;
    }

    public void setIdAudTab(long idAudTab) {
        this.idAudTab = idAudTab;
    }

    @Basic
    @Column(name = "esq_aud")
    public String getEsqAud() {
        return esqAud;
    }

    public void setEsqAud(String esqAud) {
        this.esqAud = esqAud;
    }

    @Basic
    @Column(name = "tab_aud")
    public String getTabAud() {
        return tabAud;
    }

    public void setTabAud(String tabAud) {
        this.tabAud = tabAud;
    }

    @Basic
    @Column(name = "tipo_ope")
    public String getTipoOpe() {
        return tipoOpe;
    }

    public void setTipoOpe(String tipoOpe) {
        this.tipoOpe = tipoOpe;
    }

    @Basic
    @Column(name = "fch_ope")
    public Timestamp getFchOpe() {
        return fchOpe;
    }

    public void setFchOpe(Timestamp fchOpe) {
        this.fchOpe = fchOpe;
    }

    @Basic
    @Column(name = "val_ant")
    public String getValAnt() {
        return valAnt;
    }

    public void setValAnt(String valAnt) {
        this.valAnt = valAnt;
    }

    @Basic
    @Column(name = "val_post")
    public String getValPost() {
        return valPost;
    }

    public void setValPost(String valPost) {
        this.valPost = valPost;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidAudTab bidAudTab = (BidAudTab) o;
        return idAudTab == bidAudTab.idAudTab &&
                Objects.equals(esqAud, bidAudTab.esqAud) &&
                Objects.equals(tabAud, bidAudTab.tabAud) &&
                Objects.equals(tipoOpe, bidAudTab.tipoOpe) &&
                Objects.equals(fchOpe, bidAudTab.fchOpe) &&
                Objects.equals(valAnt, bidAudTab.valAnt) &&
                Objects.equals(valPost, bidAudTab.valPost) &&
                Objects.equals(usrCrea, bidAudTab.usrCrea) &&
                Objects.equals(fchCrea, bidAudTab.fchCrea) &&
                Objects.equals(usrModi, bidAudTab.usrModi) &&
                Objects.equals(fchModi, bidAudTab.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idAudTab, esqAud, tabAud, tipoOpe, fchOpe, valAnt, valPost, usrCrea, fchCrea, usrModi, fchModi);
    }
}
