package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidMuniPK implements Serializable {
    private String idPais;
    private String idEstd;
    private String idMuni;

    @Column(name = "id_pais")
    @Id
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Column(name = "id_estd")
    @Id
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Column(name = "id_muni")
    @Id
    public String getIdMuni() {
        return idMuni;
    }

    public void setIdMuni(String idMuni) {
        this.idMuni = idMuni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidMuniPK bidMuniPK = (BidMuniPK) o;
        return Objects.equals(idPais, bidMuniPK.idPais) &&
                Objects.equals(idEstd, bidMuniPK.idEstd) &&
                Objects.equals(idMuni, bidMuniPK.idMuni);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd, idMuni);
    }
}
