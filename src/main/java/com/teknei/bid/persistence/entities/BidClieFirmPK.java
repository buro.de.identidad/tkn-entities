package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieFirmPK implements Serializable {
    private Long idClie;
    private Long idTipoClie;
    private Long idTipoFirm;

    @Column(name = "id_clie")
    @Id
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_tipo_clie")
    @Id
    public Long getIdTipoClie() {
        return idTipoClie;
    }

    public void setIdTipoClie(Long idTipoClie) {
        this.idTipoClie = idTipoClie;
    }

    @Column(name = "id_tipo_firm")
    @Id
    public Long getIdTipoFirm() {
        return idTipoFirm;
    }

    public void setIdTipoFirm(Long idTipoFirm) {
        this.idTipoFirm = idTipoFirm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieFirmPK that = (BidClieFirmPK) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(idTipoClie, that.idTipoClie) &&
                Objects.equals(idTipoFirm, that.idTipoFirm);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idTipoClie, idTipoFirm);
    }
}
