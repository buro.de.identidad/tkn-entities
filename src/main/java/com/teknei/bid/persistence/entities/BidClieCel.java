package com.teknei.bid.persistence.entities;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "bid_clie_cel", schema = "bid", catalog = "bid")
public class BidClieCel {
	
	private Long idClie;
    private String numCel;
	

    @Basic
    @Id
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "num_cel")
    public String getCel() {
        return numCel;
    }

    public void setCel(String numCel) {
        this.numCel = numCel;
    }
    
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCel that = (BidClieCel) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(numCel, that.numCel);
}
    @Override
    public int hashCode() {

        return Objects.hash(idClie, numCel);
        		}
}
