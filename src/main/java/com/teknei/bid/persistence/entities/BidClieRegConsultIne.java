package com.teknei.bid.persistence.entities;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

@Entity
@Table(name = "bid_clie_reg_consult_ine", schema = "bid", catalog = "bid")
@TypeDefs({ @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class) })
public class BidClieRegConsultIne {

	private Long id;
	private Long idClie;// int8 NOT NULL, -- IDENTIFICADOR DEL CLIENTE
	private String dataRequest;// json NULL,
	private Boolean regbiom;// bool NULL DEFAULT false, -- Registro del biometrico
	private Boolean reginfo;// bool NULL DEFAULT false, -- En caso de que la informacion este bien
	private Integer intento;// int8 NOT NULL DEFAULT 0, -- Numero de intentos, m�ximo 5
	private Timestamp registerDate;// timestamp NULL DEFAULT now(), -- Fecha de registro
	private Timestamp lastUpdateDate;// timestamp NULL, -- �ltima fecha de actualizaci�n
	private Boolean sent;// bool NULL DEFAULT false, -- En caso de que la informacion este bien

	@Id
	@SequenceGenerator(schema = "bid", name = "BID_REG_CONSULT_INE_SEQ", sequenceName = "bid.bid_clie_reg_consult_ine_seq", allocationSize = 1, catalog = "bid")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_REG_CONSULT_INE_SEQ")
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "id_clie")
	public Long getIdClie() {
		return idClie;
	}

	public void setIdClie(Long idClie) {
		this.idClie = idClie;
	}

	@Basic
	@Type(type = "jsonb")
	@Column(name = "data_request", columnDefinition = "json")
	public String getDataRequest() {
		return dataRequest;
	}

	public void setDataRequest(String dataRequest) {
		this.dataRequest = dataRequest;
	}

	@Basic
	@Column(name = "regbiom")
	public Boolean getRegbiom() {
		return regbiom;
	}

	public void setRegbiom(Boolean regbiom) {
		this.regbiom = regbiom;
	}

	@Basic
	@Column(name = "reginfo")
	public Boolean getReginfo() {
		return reginfo;
	}

	public void setReginfo(Boolean reginfo) {
		this.reginfo = reginfo;
	}

	@Basic
	@Column(name = "intento")
	public Integer getIntento() {
		return intento;
	}

	public void setIntento(Integer intento) {
		this.intento = intento;
	}

	@Basic
	@Column(name = "register_date")
	public Timestamp getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}

	@Basic
	@Column(name = "last_update_date")
	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	@Basic
	@Column(name = "sent")
	public Boolean getSent() {
		return sent;
	}

	public void setSent(Boolean sent) {
		this.sent = sent;
	}

}
