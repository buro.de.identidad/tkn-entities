package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_ife_ine", schema = "bid", catalog = "bid")
@IdClass(BidClieIfeInePK.class)
public class BidClieIfeIne {
    private long idClie;
    private long idIfe;
    private String nomb;
    private String apePate;
    private String apeMate;
    private String call;
    private String noExt;
    private String noInt;
    private String col;
    private String cp;
    private String foli;
    private String clavElec;
    private String ocr;
    private String esta;
    private String muni;
    private String dist;
    private String loca;
    private String secc;
    private Timestamp vige;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String mrz;
    private String curp;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_ife")
    public long getIdIfe() {
        return idIfe;
    }

    public void setIdIfe(long idIfe) {
        this.idIfe = idIfe;
    }

    @Basic
    @Column(name = "nomb")
    public String getNomb() {
        return nomb;
    }

    public void setNomb(String nomb) {
        this.nomb = nomb;
    }

    @Basic
    @Column(name = "ape_pate")
    public String getApePate() {
        return apePate;
    }

    public void setApePate(String apePate) {
        this.apePate = apePate;
    }

    @Basic
    @Column(name = "ape_mate")
    public String getApeMate() {
        return apeMate;
    }

    public void setApeMate(String apeMate) {
        this.apeMate = apeMate;
    }

    @Basic
    @Column(name = "call")
    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    @Basic
    @Column(name = "no_ext")
    public String getNoExt() {
        return noExt;
    }

    public void setNoExt(String noExt) {
        this.noExt = noExt;
    }

    @Basic
    @Column(name = "no_int")
    public String getNoInt() {
        return noInt;
    }

    public void setNoInt(String noInt) {
        this.noInt = noInt;
    }

    @Basic
    @Column(name = "col")
    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    @Basic
    @Column(name = "cp")
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Basic
    @Column(name = "foli")
    public String getFoli() {
        return foli;
    }

    public void setFoli(String foli) {
        this.foli = foli;
    }

    @Basic
    @Column(name = "clav_elec")
    public String getClavElec() {
        return clavElec;
    }

    public void setClavElec(String clavElec) {
        this.clavElec = clavElec;
    }

    @Basic
    @Column(name = "ocr")
    public String getOcr() {
        return ocr;
    }

    public void setOcr(String ocr) {
        this.ocr = ocr;
    }

    @Basic
    @Column(name = "esta")
    public String getEsta() {
        return esta;
    }

    public void setEsta(String esta) {
        this.esta = esta;
    }

    @Basic
    @Column(name = "muni")
    public String getMuni() {
        return muni;
    }

    public void setMuni(String muni) {
        this.muni = muni;
    }

    @Basic
    @Column(name = "dist")
    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    @Basic
    @Column(name = "loca")
    public String getLoca() {
        return loca;
    }

    public void setLoca(String loca) {
        this.loca = loca;
    }

    @Basic
    @Column(name = "secc")
    public String getSecc() {
        return secc;
    }

    public void setSecc(String secc) {
        this.secc = secc;
    }

    @Basic
    @Column(name = "vige")
    public Timestamp getVige() {
        return vige;
    }

    public void setVige(Timestamp vige) {
        this.vige = vige;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "mrz")
    public String getMrz() {
        return mrz;
    }

    public void setMrz(String mrz) {
        this.mrz = mrz;
    }

    @Basic
    @Column(name = "curp")
    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieIfeIne that = (BidClieIfeIne) o;
        return idClie == that.idClie &&
                idIfe == that.idIfe &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(nomb, that.nomb) &&
                Objects.equals(apePate, that.apePate) &&
                Objects.equals(apeMate, that.apeMate) &&
                Objects.equals(call, that.call) &&
                Objects.equals(noExt, that.noExt) &&
                Objects.equals(noInt, that.noInt) &&
                Objects.equals(col, that.col) &&
                Objects.equals(cp, that.cp) &&
                Objects.equals(foli, that.foli) &&
                Objects.equals(clavElec, that.clavElec) &&
                Objects.equals(ocr, that.ocr) &&
                Objects.equals(esta, that.esta) &&
                Objects.equals(muni, that.muni) &&
                Objects.equals(dist, that.dist) &&
                Objects.equals(loca, that.loca) &&
                Objects.equals(secc, that.secc) &&
                Objects.equals(vige, that.vige) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(mrz, that.mrz) &&
                Objects.equals(curp, that.curp) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idIfe, nomb, apePate, apeMate, call, noExt, noInt, col, cp, foli, clavElec, ocr, esta, muni, dist, loca, secc, vige, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, mrz, curp, usrOpeCrea, usrOpeModi);
    }
}
